from tkinter import *  

# этот код открывает окно с двумя активными кнопками

def clicked():  
    lbl.configure(text="Я же просил...")  

def clicked1():  
    lbl.configure(text="YOUR COMPUTER WILL RESTART NOW!")    
  
window = Tk()  
window.title("POMOGAIKA v 2.0")  
window.geometry('600x400')  


lbl = Label(window, text=" some text ", font=("Arial Bold", 16))  
lbl.grid(column=0, row=0)  
btn = Button(window, text=" PUSH ME ", command=clicked)  
btn.grid(column=0, row=1)  

lbl = Label(window, text="    RESTART COMPUTER    ", font=("Arial Bold", 16))  
lbl.grid(column=1, row=0)  
btn = Button(window, text=" PUSH ME ", command=clicked1)  
btn.grid(column=1, row=1) 



# Этот код вызывает одно окно с кнопкой
#  при нажатии кнопки открывается новое окно (почему-то 2 окна) сверху

class Window(Tk):
    def __init__(self, parent):
        Tk.__init__(self, parent)
        self.parent = parent
        self.initialize()

    def initialize(self):
        self.geometry("600x400+30+30")
        self.wButton = Button(self, text='text', command = self.OnButtonClick)
        self.wButton.pack()

    def OnButtonClick(self):
        self.top = Toplevel()
        self.top.title("title")
        self.top.geometry("300x150+30+30")
        self.top.transient(self)
        self.wButton.config(state='disabled')

        self.topButton = Button(self.top, text="CLOSE", command = self.OnChildClose)
        self.topButton.pack()

    def OnChildClose(self):
        self.wButton.config(state='normal')
        self.top.destroy()

if __name__ == "__main__":
    window = Window(None)

    window.title("title")

window.mainloop()