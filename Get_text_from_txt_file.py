import re
import sys

# В файле saveit.txt хранима значения переменных вида:
# A=10
# B=5
# C=12
# D=14

with open('saveit.txt') as f:
    fil = f.read()
    A = int(re.search(r'\bA=(\d+)', fil).group(1)) #для переменной А ищем совпадение А= и его присваиваем
    B = int(re.search(r'\bB=(\d+)', fil).group(1))
    C = int(re.search(r'\bC=(\d+)', fil).group(1))
    D = int(re.search(r'\bD=(\d+)', fil).group(1))
    
    #просто для примера выполняем действия с полученными переменными
    Value1= A+2*B
    Value2=D-B+C


    with open("out.txt","w") as out:
        print(Value1, Value2,file=out) #сохраняет результат в файл out.txt
    # print (Value1, Value2) #просто выводит результат на экран